#!/bin/bash
set -e

# force the linker to resolve all the dynamic symbols at startup
export LD_BIND_NOW=on

compress=true
if [ "${@:1:1}" = "--nocompress" ]; then
  compress=false
  shift
fi

# if the traced program has parameters the command must be quoted
tracedCommand="${@: -1}"
# PIN cannot execute the file if it does not have the path prepended
if [ ${tracedCommand:0:1} != "/" ]; then
  tracedCommand="./$tracedCommand"
fi
thisScriptDir="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
pin="$thisScriptDir/../pin/pin"
pinToolOptionsCount=$(($# - 1))
pinToolOptions=${@:1:$pinToolOptionsCount}

outputFilesPrefix=$(echo "$pinToolOptions" | sed -ne "s/.*-o \(.*\).*/\1/p")
if [ -z "$outputFilesPrefix" ]; then
  mkdir -p dumps
  outputFilesPrefix=dumps/$(basename $(echo "$tracedCommand" | awk '{print $1}'))
  pinToolOptions="$pinToolOptions -o $outputFilesPrefix"
fi

set +e
"$pin" -injection child -t "$thisScriptDir/obj-intel64/tracer.so" $pinToolOptions -- $tracedCommand
set -e

if [ $compress = "true" ]; then
  dumpsDirectory=$(dirname "$outputFilesPrefix")
  outputFilesPrefix=$(basename "$outputFilesPrefix")
  cd "$dumpsDirectory"
  outputFilesExtensions="info segments flow heap malloc registers stack debug"
  for ext in $outputFilesExtensions; do
    traceFile="$outputFilesPrefix.$ext"
    for file in "$traceFile"*; do
      if [ -e "$file" ]; then
	traceFiles="$traceFiles $file"
      fi
    done
  done
  if [ -z "$traceFiles" ]; then
    echo "No trace files produced, exiting."
    exit 1
  fi
  
  traceOutputFile="$outputFilesPrefix.trace.tgz"
  if [ -e "$traceOutputFile" ]; then
    rm -rf "$traceOutputFile"
  fi
  tar -czf "$traceOutputFile" $traceFiles
  rm -rf $traceFiles
fi
