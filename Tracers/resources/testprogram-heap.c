#include <stdlib.h>

int fun (int *param) {
  return 34 + *param;
}

int main (int argc, char** argv) {
 int *param = malloc(sizeof(int));
 int ret;
 if (argc > 5)
  *param = 10;
 else
  *param = 11;
 ret = fun(param);
 free(param);
 return ret;
}
