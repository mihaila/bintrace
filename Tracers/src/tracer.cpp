#include "tracerUtils.h"
#include "Dumper.h"
#include "debugDump.cpp"
#include "jmpsDump.cpp"
#include "stackDump.cpp"
#include "imageDump.cpp"
#include "registersDump.cpp"
#include "heapDump.cpp"
#include "mallocDump.cpp"
#include <vector>

using namespace std;

/**
 * Command line switches.
 */
KNOB<string> ParamOutputFile(KNOB_MODE_WRITEONCE, "pintool", "o", "trace.dump",
		"Specify file name for the trace output.");
KNOB<BOOL> ParamDebug(KNOB_MODE_WRITEONCE, "pintool", "debug_control_flow", "0",
		"Print the control flow trace as text and add some debug output to the file.");
KNOB<BOOL> ParamDumpFlow(KNOB_MODE_WRITEONCE, "pintool", "control_flow", "1",
		"Dump the control flow to a separate file (<tracefile> + .flow).");
KNOB<BOOL> ParamDumpCode(KNOB_MODE_WRITEONCE, "pintool", "code", "1",
		"Dump the code segments content to a separate file (<tracefile> + .segments).");
KNOB<BOOL> ParamDumpData(KNOB_MODE_WRITEONCE, "pintool", "data", "1",
		"Dump the data segments content to a separate file (<tracefile> + .segments).");
KNOB<BOOL> ParamDumpStack(KNOB_MODE_WRITEONCE, "pintool", "stack", "1",
		"Dump the stack content to a separate file (<tracefile> + .stack).");
KNOB<BOOL> ParamDumpHeap(KNOB_MODE_WRITEONCE, "pintool", "heap", "1",
		"Dump the heap content to a separate file (<tracefile> + .heap).");
KNOB<BOOL> ParamDumpRegisters(KNOB_MODE_WRITEONCE, "pintool", "registers", "1",
		"Dump the registers contents to a separate file (<tracefile> + .regs).");
KNOB<BOOL> ParamDumpMallocs(KNOB_MODE_WRITEONCE, "pintool", "malloc", "1",
		"Dump the \"malloc\" and \"free\" calls to a separate file (<tracefile> + .malloc).");
KNOB<string> ParamTraceStartPoint(KNOB_MODE_WRITEONCE, "pintool", "start", "",
		"Specify the name of a function or an address in hex (e.g. 0x00ac09) where to begin the tracing at.");
KNOB<string> ParamTraceStopPoint(KNOB_MODE_WRITEONCE, "pintool", "stop", "",
		"Specify the name of a function or an address in hex (e.g. 0x00fb75) where to end the tracing at.");

THREADID mainThreadID = 0;
bool mainThreadSeen = false;
traces::TraceInfo traceInfo;
INSTLIB::ALARM_ADDRESS_COUNT addressAlarmStartPoint(true);
INSTLIB::ALARM_SYMBOL_COUNT symbolAlarmStartPoint(true);
INSTLIB::ALARM_ADDRESS_COUNT addressAlarmStopPoint(true);
INSTLIB::ALARM_SYMBOL_COUNT symbolAlarmStopPoint(true);

vector<Dumper*> dumpTools;

static void initTools () {
	dumpTools.push_back(new ImageDumper(ParamDumpCode.Value(), ParamDumpData.Value()));
	if (ParamDebug.Value())
		dumpTools.push_back(new DebugDumper());
	if (ParamDumpFlow.Value())
		dumpTools.push_back(new JmpsDumper());
	if (ParamDumpStack.Value())
		dumpTools.push_back(new StackDumper());
	if (ParamDumpRegisters.Value())
		dumpTools.push_back(new RegistersDumper());
	if (ParamDumpHeap.Value())
		dumpTools.push_back(new HeapDumper());
	if (ParamDumpMallocs.Value())
		dumpTools.push_back(new MallocDumper());
	const string& outputFileName = ParamOutputFile.Value();
	for (vector<Dumper*>::iterator iter = dumpTools.begin(); iter != dumpTools.end(); ++iter) {
		(*iter)->init(outputFileName);
	}
}

static void dumpTraceInfo () {
	const string& outputFileName = ParamOutputFile.Value();
	string dumpFileName(outputFileName + ".info");
	ofstream dumpFile;
	dumpFile.open(dumpFileName.c_str());
	traceInfo.SerializeToOstream(&dumpFile);
}

static void startTools (THREADID threadID, const CONTEXT* ctx) {
	traceInfo.set_startaddress(getProgramCounterValue(ctx));
	for (vector<Dumper*>::iterator iter = dumpTools.begin(); iter != dumpTools.end(); ++iter) {
		(*iter)->startTracing(threadID, ctx);
	}
}

static void stopTools (THREADID threadID, const CONTEXT* ctx) {
	traceInfo.set_stopaddress(getProgramCounterValue(ctx));
	dumpTraceInfo();
	for (vector<Dumper*>::iterator iter = dumpTools.begin(); iter != dumpTools.end(); ++iter) {
		(*iter)->stopTracing(threadID, ctx);
	}
}

static void onStartTrigger (void* payload, CONTEXT* ctx, void* pc, THREADID threadID) {
	startTools(threadID, ctx);
	mainThreadID = threadID;
}

static void onThreadStart (THREADID threadID, CONTEXT* ctx, INT32 exitCode, void* payload) {
	if (!mainThreadSeen) {
		mainThreadSeen = true;
		mainThreadID = threadID;
		startTools(threadID, ctx);
	}
}

static void onStopTrigger (void* payload, CONTEXT* ctx, void* pc, THREADID threadID) {
	stopTools(threadID, ctx);
	PIN_ExitApplication(0);
}

static void onThreadExit (THREADID threadID, const CONTEXT* ctx, INT32 exitCode, void* payload) {
	if (threadID == mainThreadID) {
		stopTools(threadID, ctx);
	}
}

/**
 * This function is called when the application exits.
 * @param[in]   code            exit code of the application
 * @param[in]   payload         value specified by the tool in the PIN_AddFiniFunction function call
 */
static void onProgramExit (INT32 exitCode, void* payload) {
}

static void initStartPoint () {
	string hexPrefix("0x");
	const string& startPoint = ParamTraceStartPoint.Value();
	if (startPoint != "") {
		if (startPoint.compare(0, hexPrefix.size(), hexPrefix)) {
			symbolAlarmStartPoint.Activate(startPoint.c_str());
			symbolAlarmStartPoint.SetAlarm(0, onStartTrigger, 0);
		} else {
			ADDRINT startAddress = parseHex(startPoint);
			addressAlarmStartPoint.Activate(startAddress);
			addressAlarmStartPoint.SetAlarm(0, onStartTrigger, 0);
		}
	} else { // add instrumentation at program start
		PIN_AddThreadStartFunction(onThreadStart, 0);
	}
}

static void initStopPoint () {
	string hexPrefix("0x");
	const string& stopPoint = ParamTraceStopPoint.Value();
	if (stopPoint != "") {
		if (stopPoint.compare(0, hexPrefix.size(), hexPrefix)) {
			symbolAlarmStopPoint.Activate(stopPoint.c_str());
			symbolAlarmStopPoint.SetAlarm(0, onStopTrigger, 0);
		} else {
			ADDRINT stopAddress = parseHex(stopPoint);
			addressAlarmStopPoint.Activate(stopAddress);
			addressAlarmStopPoint.SetAlarm(0, onStopTrigger, 0);
		}
	} else { // stop instrumentation only at program end
		PIN_AddThreadFiniFunction(onThreadExit, 0);
	}
}

static void init () {
	PIN_InitSymbols();
	initTools();
	initStartPoint();
	initStopPoint();
	PIN_AddFiniFunction(onProgramExit, 0); // Register function to be called when the application exits
}

/**
 *  Print out help message.
 */
static int usage () {
	cerr << "This tool prints out the trace of jumps." << endl;
	cerr << KNOB_BASE::StringKnobSummary() << endl;
	return -1;
}

/**
 * The main procedure of the tool. This function is called when the application image is loaded but not yet started.
 * @param[in]   argc            total number of elements in the argv array
 * @param[in]   argv            array of command line arguments, including pin -t <toolname> -- ...
 */
int main (int argc, char* argv[]) {
	if (PIN_Init(argc, argv))
		return usage();
	init();
	PIN_StartProgram(); // Start the program, never returns
	return 0;
}
