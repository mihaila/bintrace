#include "tracerUtils.h"
#include "Dumper.h"
#include <vector>

using namespace std;

struct mallocCall {
	ADDRINT callSite;
	ADDRINT returnSite;
	ADDRINT sizeArgument;
	ADDRINT returnValue;
};

struct freeCall {
	ADDRINT callSite;
	ADDRINT returnSite;
	ADDRINT pointerArgument;
};

class RecursiveCallsLock {
	ADDRINT lock;

public:
	RecursiveCallsLock () {
		lock = 0;
	}

	void tryLockAquire (const CONTEXT* ctx) {
		if (isUnlocked()) {
			ADDRINT stackPointerValue = PIN_GetContextReg(ctx, REG_STACK_PTR);
			lock = stackPointerValue;
		}
	}

	bool haveLock (const CONTEXT* ctx) {
		ADDRINT stackPointerValue = PIN_GetContextReg(ctx, REG_STACK_PTR);
		return lock == stackPointerValue;
	}

	void releaseLock () {
		lock = 0;
	}

	bool isUnlocked () {
		return lock == 0;
	}
};

class MallocDumper: public virtual Dumper {
	static const char* mallocFunctionName;
	static const char* freeFunctionName;
	static const REG parameterRegister = REG_GDI;
	static const REG returnRegister = REG_GAX;
	ADDRINT addressOfMalloc;
	ADDRINT addressOfFree;
	mallocCall lastMallocCall;
	freeCall lastFreeCall;
	RecursiveCallsLock mallocsLock;
	RecursiveCallsLock freesLock;
	mallocTrace trace;
	ofstream mallocDumpFile;

public:
	MallocDumper () {
		addressOfMalloc = 0;
		addressOfFree = 0;
	}

	~MallocDumper () {
		mallocDumpFile.close();
	}

	void init (string outputFileNamePrefix) {
		mallocDumpFile.open((outputFileNamePrefix + ".malloc").c_str());
		// instrumentation has to be initialized here already to catch the loading of the main executable image
		// TODO: implement the "ignore loader" option and then see how to catch the malloc in static executables
		instrument();
	}

	void checkPointDump (string dumpSuffix, THREADID threadID, const CONTEXT* ctx) {
	}

	void startTracing (THREADID threadID, const CONTEXT* ctx) {
		// TODO: instrument only from here on? see how to do it if needed.
	}

	void stopTracing (THREADID threadID, const CONTEXT* ctx) {
		trace.serialize(mallocDumpFile);
	}

private:
	void recordLastMallocCall () {
		trace.addMallocCall(lastMallocCall.callSite, lastMallocCall.returnSite, lastMallocCall.sizeArgument, lastMallocCall.returnValue);
	}

	void recordLastFreeCall () {
		trace.addFreeCall(lastFreeCall.callSite, lastFreeCall.returnSite, lastFreeCall.pointerArgument);
	}

	void instrument () {
		IMG_AddInstrumentFunction(onImageLoad, this);
		INS_AddInstrumentFunction(instrumentInstructions, this);
	}

	static void onImageLoad (IMG image, void* dumperObject) {
		MallocDumper* dumper = static_cast<MallocDumper*>(dumperObject);
		RTN mallocFunction = RTN_FindByName(image, mallocFunctionName);
		if (RTN_Valid(mallocFunction)) {
			dumper->addressOfMalloc = RTN_Address(mallocFunction);
			replaceMalloc(mallocFunction, dumperObject);
//			LOG("malloc found in file: " + IMG_Name(image) + "\n");
//			LOG("malloc address: " + toHex(dumper->addressOfMalloc) + "\n");
		}
		RTN freeFunction = RTN_FindByName(image, freeFunctionName);
		if (RTN_Valid(freeFunction)) {
			dumper->addressOfFree = RTN_Address(freeFunction);
			replaceFree(freeFunction, dumperObject);
//			LOG("free found in file: " + IMG_Name(image) + "\n");
//			LOG("free address: " + toHex(dumper->addressOfFree) + "\n");
		}
	}

	static void* mallocWrapper (AFUNPTR originalMalloc, const CONTEXT* ctx, ADDRINT returnAddress,
			size_t allocationSize, void* dumperObject) {
		MallocDumper* dumper = static_cast<MallocDumper*>(dumperObject);
		dumper->mallocsLock.tryLockAquire(ctx);
		void* pointer;
		PIN_CallApplicationFunction(ctx, PIN_ThreadId(), CALLINGSTD_DEFAULT, originalMalloc, PIN_PARG(void*), &pointer,
				PIN_PARG(size_t), allocationSize, PIN_PARG_END());
		if (dumper->mallocsLock.haveLock(ctx)) {
			dumper->lastMallocCall.sizeArgument = allocationSize;
			dumper->lastMallocCall.returnSite = returnAddress;
			dumper->lastMallocCall.returnValue = reinterpret_cast<ADDRINT>(pointer);
			dumper->recordLastMallocCall();
			dumper->mallocsLock.releaseLock();
		}
		return pointer;
	}

	static void freeWrapper (AFUNPTR originalFree, const CONTEXT* ctx, ADDRINT returnAddress, void* pointer,
			void* dumperObject) {
		MallocDumper* dumper = static_cast<MallocDumper*>(dumperObject);
		dumper->freesLock.tryLockAquire(ctx);
		PIN_CallApplicationFunction(ctx, PIN_ThreadId(), CALLINGSTD_DEFAULT, originalFree, PIN_PARG(void),
				PIN_PARG(void*), pointer, PIN_PARG_END());
		if (dumper->freesLock.haveLock(ctx)) {
			dumper->lastFreeCall.returnSite = returnAddress;
			dumper->lastFreeCall.pointerArgument = reinterpret_cast<ADDRINT>(pointer);
			dumper->recordLastFreeCall();
			dumper->freesLock.releaseLock();
		}
	}

	static void replaceMalloc (RTN mallocFunction, void* dumperObject) {
		PROTO mallocProtoype = PROTO_Allocate(PIN_PARG(void*), CALLINGSTD_DEFAULT, mallocFunctionName, PIN_PARG(size_t),
				PIN_PARG_END());
		RTN_ReplaceSignature(mallocFunction, (AFUNPTR) mallocWrapper, IARG_PROTOTYPE, mallocProtoype, IARG_ORIG_FUNCPTR,
				IARG_CONST_CONTEXT, IARG_RETURN_IP, IARG_FUNCARG_ENTRYPOINT_VALUE, 0, IARG_PTR, dumperObject, IARG_END);
	}

	static void replaceFree (RTN freeFunction, void* dumperObject) {
		PROTO freeProtoype = PROTO_Allocate(PIN_PARG(void), CALLINGSTD_DEFAULT, freeFunctionName, PIN_PARG(void*),
				PIN_PARG_END());
		RTN_ReplaceSignature(freeFunction, (AFUNPTR) freeWrapper, IARG_PROTOTYPE, freeProtoype, IARG_ORIG_FUNCPTR,
				IARG_CONST_CONTEXT, IARG_RETURN_IP, IARG_FUNCARG_ENTRYPOINT_VALUE, 0, IARG_PTR, dumperObject, IARG_END);
	}

	static void possibleMallocFreeCall (ADDRINT ip, ADDRINT target, void* dumperObject) {
		MallocDumper* dumper = static_cast<MallocDumper*>(dumperObject);
		if (target == dumper->addressOfMalloc) {
			if (dumper->mallocsLock.isUnlocked()) {
				dumper->lastMallocCall.callSite = ip;
			}
		} else if (target == dumper->addressOfFree) {
			if (dumper->freesLock.isUnlocked()) {
				dumper->lastFreeCall.callSite = ip;
			}
		}
	}

	static void instrumentInstructions (INS insn, void* dumperObject) {
		// assumes that malloc/free are not reached through fall-through control transfer but only through calls/jumps
		if (INS_IsBranchOrCall(insn)) {
			INS_InsertCall(insn, IPOINT_BEFORE, (AFUNPTR) possibleMallocFreeCall, IARG_INST_PTR,
					IARG_BRANCH_TARGET_ADDR, IARG_PTR, dumperObject, IARG_END);
		}
	}
};

const char* MallocDumper::mallocFunctionName = "malloc";
const char* MallocDumper::freeFunctionName = "free";
