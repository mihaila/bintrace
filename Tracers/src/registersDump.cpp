#include "tracerUtils.h"
#include "Dumper.h"
#include <map>

class RegistersDumper: public virtual Dumper {
	string dumpFileName;

public:
	void init (string outputFileNamePrefix) {
		dumpFileName = outputFileNamePrefix + ".registers";
	}

	void checkPointDump (string dumpSuffix, THREADID threadID, const CONTEXT* ctx) {
		string fileName(dumpFileName + "." + dumpSuffix);
		dumpToFile(fileName, ctx);
	}

	void startTracing (THREADID threadID, const CONTEXT* ctx) {
		dumpToFile(dumpFileName, ctx);
	}

	void stopTracing (THREADID threadID, const CONTEXT* ctx) {
	}

private:
	static void dumpToFile (string fileName, const CONTEXT* ctx) {
		ofstream dumpFile;
		dumpFile.open(fileName.c_str());
		registerValues* registers = saveRegistersContents(ctx);
		registers->serialize(dumpFile);
		dumpFile.close();
		delete registers;
	}

	static registerValues* saveRegistersContents (const CONTEXT* ctx) {
		vector<registerValues*> registers;
		registerValues* registersDump = new registerValues;
		REGSET registerSet = getInterestedRegisters();
		int regsNumber = REGSET_PopCount(registerSet);
		registersDump->programAddress = getProgramCounterValue(ctx);
		for (int i = 0; i < regsNumber; i++) {
			REG reg = REGSET_PopNext(registerSet);
			ADDRINT value = PIN_GetContextReg(ctx, reg);
			string regName = REG_StringShort(reg);
			registersDump->regValues[regName] = value;
		}
		return registersDump;
	}

	static REGSET getInterestedRegisters () {
		REGSET regs;
		for (int i = REG_FirstInRegset; i <= REG_LastInRegset; i++) {
			REG reg = REG(i);
			if (REG_valid(reg) && (REG_is_gr(reg) || REG_is_br(reg) || REG_is_seg(reg)))
				REGSET_Insert(regs, reg);
		}
		// TODO: see if we need the flags to bootstrap an analysis and how to translate/insert them on the analysis side
		//REGSET_Insert(regs, REG_GFLAGS);
		return regs;
	}

};

