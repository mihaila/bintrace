#include "tracerUtils.h"
#include "Dumper.h"

using namespace std;

class ImageDumper: public virtual Dumper {
	bool dumpCode;
	bool dumpData;
	string dumpFileName;

public:
	ImageDumper (bool dumpCode = true, bool dumpData = true) {
		this->dumpCode = dumpCode;
		this->dumpData = dumpData;
	}

	void init (string outputFileNamePrefix) {
		dumpFileName = outputFileNamePrefix + ".segments";
	}

	void checkPointDump (string dumpSuffix, THREADID threadID, const CONTEXT* ctx) {
		string fileName(dumpFileName + "." + dumpSuffix);
		dumpToFile(fileName);
	}

	void startTracing (THREADID threadID, const CONTEXT* ctx) {
		dumpToFile(dumpFileName);
	}

	void stopTracing (THREADID threadID, const CONTEXT* ctx) {
	}

private:
	void dumpToFile (string fileName) {
		module segments;
		dumpAllCodeAndData(segments);
		dumpLinuxSpecialDSOs(segments);
		ofstream dumpFile;
		dumpFile.open(fileName.c_str());
		segments.serialize(dumpFile);
		dumpFile.close();
	}

	// Linux fast syscall interfaces
	void dumpLinuxSpecialDSOs(module& segments) {
		segment* vdso = dumpNamedSegmentFromMemMap("vdso");
		segment* vsyscall = dumpNamedSegmentFromMemMap("vsyscall");
		if (vdso)
			segments.add(vdso);
		if (vsyscall)
			segments.add(vsyscall);
	}

	void dumpAllCodeAndData (module& segments) {
		for (IMG img = APP_ImgHead(); IMG_Valid(img); img = IMG_Next(img)) {
			recordImage(img, segments);
		}
	}

	void recordImage (IMG img, module& segments) {
		for (SEC sec = IMG_SecHead(img); SEC_Valid(sec); sec = SEC_Next(sec)) {
			if (!SEC_Mapped(sec))
				continue;
			ADDRINT address = SEC_Address(sec);
			USIZE size = SEC_Size(sec);
			BOOL executable = SEC_IsExecutable(sec);
			BOOL readable = SEC_IsReadable(sec);
			BOOL writable = SEC_IsWriteable(sec);
			const char* dataPtr = (char*) address;
			segment* seg = new segment;
			seg->fileName = IMG_Name(img);
			seg->name = SEC_Name(sec);
			seg->address = address;
			seg->endian = getEndianness();
			seg->size = size;
			seg->permissions = seg->encodePermissions(readable, writable, executable);
			char* dataCopy = new char[size];
			PIN_SafeCopy(dataCopy, dataPtr, size);
			seg->data = dataCopy;
			if (dumpCode && executable) {
				ASSERTX(readable);
				ASSERTX(!writable);
				// we do not handle modifiable code sections yet
				segments.add(seg);
			}
			if (dumpData && !executable){
				ASSERTX(readable);
				segments.add(seg);
			}
		}
	}

};

