#include "tracerUtils.h"
#define USE_PROTOBUF

const string toHex (address_t address) {
	stringstream result;
	result << "0x" << std::hex << std::setw(12) << std::setfill('0') << address;
	return result.str();
}

address_t parseHex (string number) {
	address_t value;
	stringstream stream;
	stream << std::hex << number;
	stream >> value;
	return value;
}

char* dumpMemoryRange (addressRange range) {
	size_t size = range.upperBound - range.lowerBound + 1;
	char* memory = new char[size];
	const void* source = reinterpret_cast<const void*>(range.lowerBound);
	PIN_SafeCopy(memory, source, size);
	return memory;
}

string getMemMapFromLinuxProc () {
	ifstream memoryFile;
	memoryFile.open("/proc/self/maps", ios_base::in | ios_base::binary);
	stringstream memory;
	memory << memoryFile.rdbuf();
	return memory.str();
}

string disassembleFunction (RTN function) {
	string disassembly;
	if (!RTN_Valid(function))
		return disassembly;
	disassembly.append("Disassembly of function: " + RTN_Name(function) + "@" + toHex(RTN_Address(function)) + "\n");
	for (INS insn = RTN_InsHead(function); INS_Valid(insn); insn = INS_Next(insn)) {
		disassembly.append(INS_Disassemble(insn) + "\n");
	}
	return disassembly;
}

string dumpSections (IMG image) {
	string output;
	output.append(IMG_Name(image) + "\n");
	for (SEC sec = IMG_SecHead(image); SEC_Valid(sec); sec = SEC_Next(sec)) {
		if (!SEC_Mapped(sec))
			continue;
		const string name = SEC_Name(sec);
		SEC_TYPE type = SEC_Type(sec);
		ADDRINT address = SEC_Address(sec);
		USIZE size = SEC_Size(sec);
		BOOL executable = SEC_IsExecutable(sec);
		BOOL readable = SEC_IsReadable(sec);
		BOOL writable = SEC_IsWriteable(sec);
		string permissions;
		permissions += readable ? "r" : "-";
		permissions += writable ? "w" : "-";
		permissions += executable ? "x" : "-";
		output.append(toHex(address) + " " + toHex(size));
		output.append(" type: " + toHex(type) + " permissions: " + permissions + " " + name + "\n");
	}
	return output;
}

string dumpSymbols (IMG image) {
	string output;
	if (!IMG_Valid(image))
		return output;
	output.append("Symbols in file: " + IMG_Name(image) + "\n");
	for (SYM symbol = IMG_RegsymHead(image); SYM_Valid(symbol); symbol = SYM_Next(symbol)) {
		if (SYM_Dynamic(symbol))
			output.append("d " + toHex(SYM_Value(symbol)) + " " + SYM_Name(symbol) + "\n");
		else
			output.append("  " + toHex(SYM_Value(symbol)) + " " + SYM_Name(symbol) + "\n");
	}
	return output;
}

static string getImageType (const IMG_TYPE& type) {
	switch (type) {
	case IMG_TYPE_STATIC:
		return "static";
	case IMG_TYPE_SHARED:
		return "shared";
	case IMG_TYPE_SHAREDLIB:
		return "lib/pie";
	case IMG_TYPE_RELOCATABLE:
		return "object";
	default:
		assert(false);
		return "";
	}
}

string dumpLoadedFiles () {
	stringstream output;
	output << "Files loaded:\n";
	for (IMG img = APP_ImgHead(); IMG_Valid(img); img = IMG_Next(img)) {
		ADDRINT lowAddress = IMG_LowAddress(img);
		ADDRINT highAddress = IMG_HighAddress(img);
		ADDRINT entryAddress = IMG_Entry(img);
		unsigned int size = IMG_SizeMapped(img);
		const string filename = IMG_Name(img);
		IMG_TYPE type = IMG_Type(img);
		output << toHex(lowAddress) << "-" << toHex(highAddress);
		output << " size: " << toHex(size) << " entry: " << toHex(entryAddress);
		output << " type: " << getImageType(type) << " " << filename << endl;
	}
	return output.str();
}

ADDRINT getProgramEntryPoint () {
	for (IMG image = APP_ImgHead(); IMG_Valid(image); image = IMG_Next(image)) {
		if (IMG_IsMainExecutable(image))
			return IMG_Entry(image);
	}
	return 0;
}

endianness getEndianness () {
	return LITTLE; // FIXME: retrieve this from the OS
}

segment* dumpNamedSegmentFromMemMap (string name) {
	string memMap = getMemMapFromLinuxProc();
	addressRange memoryRange;
	bool readable = false;
	bool writable = false;
	bool executable = false;
	istringstream stream(memMap);
	string line;
	while (getline(stream, line)) {
		if (line.find("[" + name + "]") == string::npos)
			continue;
		stringstream lineStream(line);
		lineStream >> hex >> memoryRange.lowerBound;
		lineStream.ignore(1); // "-" separator
		lineStream >> hex >> memoryRange.upperBound;
		memoryRange.upperBound = memoryRange.upperBound - 1;
		lineStream.ignore(1); // the space character separator
		char flag;
		lineStream >> flag;
		if (flag != '-')
			readable = true;
		lineStream >> flag;
		if (flag != '-')
			writable = true;
		lineStream >> flag;
		if (flag != '-')
			executable = true;
		segment* seg = new segment;
		seg->name = name;
		seg->fileName = "(none)";
		seg->address = memoryRange.lowerBound;
		seg->endian = getEndianness();
		seg->size = memoryRange.getSize();
		seg->permissions = seg->encodePermissions(readable, writable, executable);
		seg->data = dumpMemoryRange(memoryRange);
		return seg;
	}
	return NULL;
}

ADDRINT getProgramCounterValue (const CONTEXT* ctx) {
	return PIN_GetContextReg(ctx, REG_INST_PTR);
}

ADDRINT getValueOnTopOfStack (const CONTEXT* ctx) {
	ADDRINT stackPointer = PIN_GetContextReg(ctx, REG_STACK_PTR);
	ADDRINT value = *reinterpret_cast<ADDRINT*>(stackPointer);
	return value;
}

int segment::encodePermissions (bool readable, bool writable, bool executable) {
	int permissions = 0;
	if (executable)
		permissions |= 1;
	if (writable)
		permissions |= 1 << 1;
	if (readable)
		permissions |= 1 << 2;
	return permissions;
}

void segment::serialize (ofstream& file) {
#ifdef USE_PROTOBUF
	serializeProtoBuffers(file);
#else
	serializeRaw(file);
#endif
}

void segment::serializeProtoBuffers (ofstream& file) {
	// Verify that the version of the library that we linked against is
	// compatible with the version of the headers we compiled against.
	GOOGLE_PROTOBUF_VERIFY_VERSION;
	traces::Module::Segment seg;
	seg.set_name(name);
	seg.set_filename(fileName);
	seg.set_permissions(permissions);
	if (endian == LITTLE)
		seg.set_endianness(traces::Module_Segment_Endianness_LITTLE);
	else
		seg.set_endianness(traces::Module_Segment_Endianness_BIG);
	seg.set_address(address);
	seg.set_size(size);
	seg.set_data(data, size);
	seg.SerializeToOstream(&file);
}

/**
 * @deprecated
 */
void segment::serializeRaw (ofstream& file) {
	file.write(reinterpret_cast<char*>(&permissions), sizeof(permissions));
	file.write(reinterpret_cast<char*>(&address), sizeof(address));
	file.write(reinterpret_cast<char*>(&size), sizeof(size));
	file.write(data, size);
}

void registerValues::serialize (ofstream& file) {
#ifdef USE_PROTOBUF
	serializeProtoBuffers(file);
#else
	serializeRaw(file);
#endif
}

void registerValues::serializeProtoBuffers (ofstream& file) {
	// Verify that the version of the library that we linked against is
	// compatible with the version of the headers we compiled against.
	GOOGLE_PROTOBUF_VERIFY_VERSION;
	traces::CPUState cpu;
	traces::CPUState::Register* pc = cpu.add_registers();
	pc->set_name("PC");
	pc->set_value(programAddress);
	for (map<string, ADDRINT>::iterator iter = regValues.begin(); iter != regValues.end(); iter++) {
		traces::CPUState::Register* reg = cpu.add_registers();
		reg->set_name(iter->first);
		reg->set_value(iter->second);
	}
	cpu.SerializeToOstream(&file);
}

/**
 * @deprecated
 */
void registerValues::serializeRaw (ofstream& file) {
	file << "PC: " << toHex(programAddress) << std::endl;
	for (map<string, ADDRINT>::iterator iter = regValues.begin(); iter != regValues.end(); iter++) {
		file << iter->first << ": " << toHex(iter->second) << std::endl;
	}
}

void module::add (segment* seg) {
	segments.push_back(seg);
}

void module::serialize (ofstream& file) {
#ifdef USE_PROTOBUF
	serializeProtoBuffers(file);
#else
	serializeRaw(file);
#endif
}

void module::serializeProtoBuffers (ofstream& file) {
	// Verify that the version of the library that we linked against is
	// compatible with the version of the headers we compiled against.
	GOOGLE_PROTOBUF_VERIFY_VERSION;
	traces::Module mod;
	for (vector<segment*>::iterator iter = segments.begin(); iter != segments.end(); ++iter) {
		segment* seg = *iter;
		traces::Module::Segment* outSeg = mod.add_segments();
		outSeg->set_name(seg->name);
		outSeg->set_filename(seg->fileName);
		outSeg->set_permissions(seg->permissions);
		if (seg->endian == LITTLE)
			outSeg->set_endianness(traces::Module_Segment_Endianness_LITTLE);
		else
			outSeg->set_endianness(traces::Module_Segment_Endianness_BIG);
		outSeg->set_address(seg->address);
		outSeg->set_size(seg->size);
		outSeg->set_data(seg->data, seg->size);
	}
	mod.SerializeToOstream(&file);
}

/**
 * @deprecated
 */
void module::serializeRaw (ofstream& file) {
	for (vector<segment*>::iterator iter = segments.begin(); iter != segments.end(); ++iter) {
		segment* seg = *iter;
		seg->serialize(file);
	}
}

void controlFlowTrace::add (ADDRINT from, ADDRINT to, bool jumpTaken) {
	controlFlowTuple* flow = new controlFlowTuple;
	flow->from = from;
	flow->to = to;
	flow->jumpTaken = jumpTaken;
	controlFlows.push_back(flow);
}

void controlFlowTrace::serialize (ofstream& file) {
#ifdef USE_PROTOBUF
	serializeProtoBuffers(file);
#else
	serializeRaw(file);
#endif
}

void controlFlowTrace::serializeProtoBuffers (ofstream& file) {
	// Verify that the version of the library that we linked against is
	// compatible with the version of the headers we compiled against.
	GOOGLE_PROTOBUF_VERIFY_VERSION;
	traces::ControlFlowTrace trace;
	for (vector<controlFlowTuple*>::iterator iter = controlFlows.begin(); iter != controlFlows.end(); ++iter) {
		controlFlowTuple* flow = *iter;
		traces::ControlFlowTrace::ControlFlow* outFlow = trace.add_jumps();
		outFlow->set_currentpc(flow->from);
		outFlow->set_nextpc(flow->to);
		outFlow->set_jumpconditionwas(flow->jumpTaken);
	}
	trace.SerializeToOstream(&file);
}

/**
 * @deprecated
 */
void controlFlowTrace::serializeRaw (ofstream& file) {
	for (vector<controlFlowTuple*>::iterator iter = controlFlows.begin(); iter != controlFlows.end(); ++iter) {
		controlFlowTuple* flow = *iter;
		file.write((char*) &flow->from, sizeof(flow->from));
		file.write((char*) &flow->to, sizeof(flow->to));
		file.write((char*) &flow->jumpTaken, sizeof(flow->jumpTaken));
	}
}

void mallocTrace::addMallocCall (ADDRINT callSite, ADDRINT returnSite, ADDRINT sizeArgument, ADDRINT returnValue) {
	traces::MallocTrace::MallocCall* call = trace.add_mallocs();
	call->set_callsite(callSite);
	call->set_returnsite(returnSite);
	call->set_sizeargument(sizeArgument);
	call->set_returnvalue(returnValue);
}

void mallocTrace::addFreeCall (ADDRINT callSite, ADDRINT returnSite, ADDRINT pointerArgument) {
	traces::MallocTrace::FreeCall* call = trace.add_frees();
	call->set_callsite(callSite);
	call->set_returnsite(returnSite);
	call->set_pointerargument(pointerArgument);
}

void mallocTrace::serialize (ofstream& file) {
	serializeProtoBuffers(file);
}

void mallocTrace::serializeProtoBuffers (ofstream& file) {
	// Verify that the version of the library that we linked against is
	// compatible with the version of the headers we compiled against.
	GOOGLE_PROTOBUF_VERIFY_VERSION;
	trace.SerializeToOstream(&file);
}
