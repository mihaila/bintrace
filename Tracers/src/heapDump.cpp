#include "tracerUtils.h"
#include "Dumper.h"

using namespace std;

class HeapDumper: public virtual Dumper {
	string dumpFileName;

public:
	void init (string outputFileNamePrefix) {
		dumpFileName = outputFileNamePrefix + ".heap";
	}

	void checkPointDump (string dumpSuffix, THREADID threadID, const CONTEXT* ctx) {
		string fileName(dumpFileName + "." + dumpSuffix);
		dumpToFile(fileName);
	}

	void startTracing (THREADID threadID, const CONTEXT* ctx) {
		dumpToFile(dumpFileName);
	}

	void stopTracing (THREADID threadID, const CONTEXT* ctx) {
	}

private:
	static void dumpToFile (string fileName) {
		ofstream dumpFile;
		dumpFile.open(fileName.c_str());
		segment* heapSegment = dumpNamedSegmentFromMemMap("heap");
		if (heapSegment) {
			heapSegment->serialize(dumpFile);
			delete heapSegment;
		}
		dumpFile.close();
	}
};
